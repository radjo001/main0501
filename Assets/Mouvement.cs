using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mouvement : MonoBehaviour
{
    public float jumpForce = 5.0f;
    public float normalSpeed = 0.03f;
    public float sprintSpeed = 0.06f;
    private bool isGrounded;
    
    void Start()
    {
        
    }

    void Update()
    {
        float speed = normalSpeed;

        // Vérifier si la touche Shift est enfoncée pour activer le sprint
        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
        {
            speed = sprintSpeed;
        }

        // Avancer joueur
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Translate(Vector3.forward * speed);
        }

        // Reculer joueur
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Translate(Vector3.back * speed);
        }

        // Tourner joueur à gauche
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(Vector3.up, -2);
        }

        // Tourner joueur à droite
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(Vector3.up, 2);
        }

        // Sauter joueur
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            GetComponent<Rigidbody>().AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            isGrounded = false;
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            isGrounded = true;
        }
    }
}